package hust.soict.globalict.aims;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.aims.threads.MemoryDaemon;

public class Aims {

	public static void main(String args[]) {
		ArrayList<Order> anOrder = new ArrayList<Order>();
		MemoryDaemon memoryUsed = new MemoryDaemon();
		
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Order Management Application: ");
			System.out.println("--------------------------------");
			System.out.println("1. Create new order");
			System.out.println("2. Add item to the order");
			System.out.println("3. Delete item by id");
			System.out.println("4. Display the items list of order");
			System.out.println("0. Exit");
			System.out.println("--------------------------------");
			System.out.print("Please choose a number: 0-1-2-3-4: -->  ");

			int choice = sc.nextInt();

			switch (choice) {
				case 1:
					System.out.println("\n1. Create new order\n");
					if(Order.getNumberOfOrders() >= 5) {
						System.out.println("Full of orders!\n");
						break;
					}
					Order newOrder = new Order();	
					anOrder.add(newOrder);
					System.out.println("Number of existing orders: " + anOrder.size() + "\n");
					System.out.println("Memory used: " + memoryUsed.getMemoryUsed());
					break;
				case 2:
					//memoryUsed.run();
					if (Order.getNumberOfOrders() == 0) {
						System.out.println("\nNo exisiting order! Please create a new order first!\n");
						break;
					}
					System.out.println("\n2. Add item to the order\n");
					System.out.println("Please enter <1. DigitalVideoDisc> / <2.CompactDisc> / <3. Book>");
					System.out.print("  --> ");
					int type = sc.nextInt();

					if (type == 1) {
						System.out.println("Please enter the information of the disc following this format");
						System.out.println("---<Title - Category - Director - Length - Cost>---");
						//System.out.print("--> ID: \n");
						//String nID = sc.next();
						sc.nextLine();
						System.out.println("--> Title: ");
						String nTitle = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Category: ");
						String nCategory = sc.nextLine();
						//sc.next();
						System.out.println("--> Director: ");
						String nDirector = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Length: ");
						int nLength = sc.nextInt();
						sc.nextLine();
						System.out.println("--> Cost: ");
						float nCost = sc.nextFloat();
						
						DigitalVideoDisc newDisc = new DigitalVideoDisc(nTitle, nCategory, nCost, nLength, nDirector);
						anOrder.get(anOrder.size() - 1).addMedia(newDisc);
					} else if (type == 2) {
						System.out.println("Please enter the compact disc information first following this format");
						System.out.println("---< Title - Category - Cost - Length - Director - Artist >---");
						sc.nextLine();
						System.out.println("--> Title: ");
						String nTitle = sc.nextLine();
						System.out.println("--> Category: ");
						String nCategory = sc.nextLine();
						System.out.println("--> Cost: ");
						float nCost = sc.nextFloat();
						System.out.println("--> Length: ");
						int nLength = sc.nextInt();
						sc.nextLine();
						System.out.println("--> Director: ");
						String nDirector = sc.nextLine();
						System.out.println("--> Artist: ");
						String nArtist = sc.nextLine();
						
						CompactDisc newCompactDisc = new CompactDisc(nTitle, nCategory, nCost, nLength, nDirector, nArtist);

						System.out.print("Enter the number of tracks in this compact disc: ");
						int numTrack = sc.nextInt();
						//sc.nextLine();

						for (int i = 0; i < numTrack; i++) {
							sc.nextLine();
							System.out.println("Input track --->");
							System.out.println("--> Track's title: ");
							String nTrack = sc.nextLine();
							//sc.nextLine();
							System.out.println("--> Track's length: ");
							int nLength1 = sc.nextInt();

							Track newTrack = new Track(nTrack, nLength1);
							newCompactDisc.addTrack(newTrack);
						}
						anOrder.get(anOrder.size() - 1).addMedia(newCompactDisc);
					} else if (type == 3) {
						System.out.println("Please enter the information of the book following this format");
						System.out.println("---<ID - Title - Category - Number of author - Authors' list - Cost>---");
						System.out.println("--> ID: \n");
						String nID = sc.next();
						sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Title: ");
						String nTitle = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Category: ");
						String nCategory = sc.nextLine();
						//sc.nextLine();
						System.out.println("--> Enter the number of authors: ");
						int numberofAuthors = sc.nextInt();
						sc.nextLine();

						System.out.println("--> Enter the authors's list");
						List<String> Authors = new ArrayList<String>();
						for(int i = 0; i < numberofAuthors; i++) {
							String nAuthor = sc.nextLine();
							Authors.add(nAuthor);
						}

						//sc.nextLine();
						System.out.println("--> Cost: ");
						float nCost = sc.nextFloat();

						Book newBook = new Book(nTitle, nCategory, nCost, Authors);
						anOrder.get(anOrder.size() - 1).addMedia(newBook);
					} else {
						System.out.println("Wrong choice!");
					}
					//memoryUsed.run();
					//sc.nextLine();
					System.out.println("Memory used: " + memoryUsed.getMemoryUsed());

					break;
				case 3:
					/*if (Order.getNumberOfOrders() == 0) {
						System.out.println("\nNo exisiting order! Please create a new order first!\n");
						break;
					}
					//int checkflag = 0;

					System.out.println("\n3. Delete item by id\n");
					System.out.print("--> Please enter the id of the media you want to delete: ");
					String ID = sc.next();
					sc.nextLine();

					anOrder.get(anOrder.size() - 1).removeMedia(ID);
					System.out.println("Memory used: " + memoryUsed.getMemoryUsed());
					*/
					break;
				case 4:
					if (Order.getNumberOfOrders() == 0) {
						System.out.println("\nNo exisiting order! Please create a new order first!\n");
						break;
					}

					int numList = 0;

					do {System.out.println("\n4. Display the items list of order\n");
					
					System.out.print("--> Enter the order list's number: ");
					numList = sc.nextInt();
					} while (numList < 1 || numList > Order.getNumberOfOrders());
					anOrder.get(numList - 1).printAnOrder();
					System.out.println("Memory used: " + memoryUsed.getMemoryUsed());
					break;
				case 0:
					System.out.println("\n0. Exit\n");
					sc.close();
					System.out.println("Exiting the program~... Have a good day!");
					System.exit(0);					
				default:
					System.out.println("\nWrong choice! Please make the right selection!\n");
					break;
			}
		}
	}
}

/*public static void main(String[] args) {
	// TODO Auto-generated method stub
	Order anOrder = new Order();
	//Create a new DVD object and set the fields
	//DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King");
	dvd1.setCategory("Animation");
	dvd1.setCost(19.95f);
	dvd1.setDirector("Roger Allers");
	dvd1.setLength(87);
	//add the dvd1 to the order
	//anOrder.addDigitalVideoDisc(dvd1);
	
	DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars");
	dvd2.setCategory("Science Fiction");
	dvd2.setCost(24.95f);
	dvd2.setDirector("George Lucas");
	dvd2.setLength(124);
	//add the dvd2 to the order
	//anOrder.addDigitalVideoDisc(dvd2);
	
	DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin");
	dvd3.setCategory("Animation");
	dvd3.setCost(18.99f);
	dvd3.setDirector("John Musker");
	dvd3.setLength(90);
	//add the dvd3 to the order
	//anOrder.addDigitalVideoDisc(dvd3);
	
	DigitalVideoDisc dvd4 = new DigitalVideoDisc("Alddin");
	dvd4.setCategory("Animation");
	dvd4.setCost(18.99f);
	dvd4.setDirector("John Musker");
	dvd4.setLength(90);
	//add the dvd3 to the order
	//anOrder.removeDigitalVideoDisc(dvd4);
	
	//anOrder.removeDigitalVideoDisc(dvd2);
	
	DigitalVideoDisc[] dvdList = {dvd2, dvd4};
	anOrder.addDigitalVideoDics(dvdList);
	
	anOrder.addDigitalVideoDisc(dvd2, dvd2);
	
	anOrder.printing();
	Order order1 = new Order();
	order1.addDigitalVideoDisc(dvd1);
	Order order2 = new Order();
	order2.addDigitalVideoDisc(dvd2);
	Order order3 = new Order();
	order3.addDigitalVideoDisc(dvd3);
	Order order4 = new Order();
	order4.addDigitalVideoDisc(dvd4);
	order4.addDigitalVideoDisc(dvd4);
	order4.addDigitalVideoDisc(dvd4);
	Order order5 = new Order();
	order5.addDigitalVideoDisc(dvd1);
	Order order6 = new Order();
	order6.addDigitalVideoDisc(dvd1);
	//System.out.print("Total Cost is: ");
	//System.out.println(anOrder.totalCost());
	

}*/

