
import javax.swing.JOptionPane;
public class CalTwoNumbers {
    public static void main (String[] args)
    {
        String strNum1, strNum2;
        double num1, num2;
       
        strNum1 = JOptionPane.showInputDialog(null,
                    "Please input the first number: ", "Input the first number",
                    JOptionPane.INFORMATION_MESSAGE);
        num1 = Double.parseDouble(strNum1);

        strNum2 = JOptionPane.showInputDialog(null,
        "Please input the first number: ", "Input the first number",
        JOptionPane.INFORMATION_MESSAGE);
        num2 = Double.parseDouble(strNum2);

        JOptionPane.showMessageDialog(null, "View the result in Terminal", "Show the result", JOptionPane.INFORMATION_MESSAGE);
        System.out.println ("Sum: " + (num1+num2));
        System.out.println ("Difference: " + (num1-num2));
        System.out.println ("Product: " + (num1*num2));
        if (num2 != 0)
        {
            System.out.println ("Quotient: " + (num1/num2) + "\n");
        }
        else 
            System.out.println ("Can not calculate the quotient because the divior is equal 0!\n");
        System.exit(0);
    }
}
