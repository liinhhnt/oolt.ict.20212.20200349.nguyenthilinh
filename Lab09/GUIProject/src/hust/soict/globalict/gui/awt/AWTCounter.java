package hust.soict.globalict.gui.awt;

import java.awt.*; // Using AWT container and component classes
import java.awt.event.*; // Using AWT event classes and listener interfaces

// An AWT program inherits from the top-level container java.awt.Frame
public class AWTCounter extends Frame implements ActionListener{
	private Label lblCount; 
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	
	// Constructor to setup GUI components and event handlers
	public AWTCounter() {
		setLayout(new FlowLayout());
			// "super" Frame, which is a Container, sets its layout to FlowLayout to arrange
			// the components from left-to-right, and flow to next row from top-to-bottom.
		lblCount = new Label("Counter");    // construct the Label component
		add(lblCount);						// "super" Frame container adds Label component
		
		tfCount = new TextField(count + "", 10); // construct the TextField component with initial text
		tfCount.setEditable(false);              // set to read-only
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount);
		
		btnCount.addActionListener(this);
			// "btnCount" is the source object that fires an ActionEvent when clicked.
			 // The source add "this" instance as an ActionEvent listener, which provides
			 // an ActionEvent handler called actionPerformed().
			 // Clicking "btnCount" invokes actionPerformed().
		
		setTitle("AWT Counter");
		setSize(250, 100);
		
		// For inspecting the Container/Components objects
		 // System.out.println(this);
		 // System.out.println(lblCount);
		 // System.out.println(tfCount);
		 // System.out.println(btnCount);
		
		setVisible(true); // "super" Frame shows
		 // System.out.println(this);
		 // System.out.println(lblCount);
		 // System.out.println(tfCount);
		 // System.out.println(btnCount);
	}
		
		//The entry main() method
	public static void main(String[] args) {
		 // Invoke the constructor to setup the GUI, by allocating an instance
		 AWTCounter app = new AWTCounter();
		 // or simply "new AWTCounter();" for an anonymous instance
	}
	
	// ActionEvent handler - Called back upon button-click.

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		++count; // Increase the counter value
		 // Display the counter value on the TextField tfCount
		tfCount.setText(count + ""); // Convert int to String

	}
	
	
}
