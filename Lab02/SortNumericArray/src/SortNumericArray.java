import java.util.Arrays;
import java.util.Scanner;

public class SortNumericArray {
    public static void main(String[] args) {
        double sum = 0;
        
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter the number of elements: ");
        
        int n = keyboard.nextInt();
        double[] arr = new double[n];
        
        System.out.println("Enter value for array elements: ");
        for(int i = 0; i< n; i++) {
            arr[i] = keyboard.nextDouble();
            sum += arr[i];
        }
        
        Arrays.sort(arr);
        
        System.out.println("The sum of the array is: " + sum);
        System.out.println("The average value of the array is: " + sum/n);
        System.out.println("The array after sorted: ");
        for(int i = 0 ; i < n;i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println("\n");
        for (int i=1000000; i>0; i--)
        	System.out.print(i + " ");
        keyboard.close();
    }
}
