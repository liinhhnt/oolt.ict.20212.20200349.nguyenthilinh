import java.util.Scanner;

public class NumberOfDays {
	public static void main(String[] strings) {
		int numberOfDays = 0;
		String month;
		int year;
		
        System.out.print("Input month: ");
		Scanner keyboard = new Scanner(System.in);
        month = keyboard.nextLine();

        System.out.print("Input year: ");
        year = keyboard.nextInt();

        switch (month) {
            case "January": case "Jan.": case "Jan": case "1":
                numberOfDays = 31;
                break;
            case "February": case "Feb.": case "Feb": case "2": 
                if (checkLeapYear(year))
                    numberOfDays = 29;
                else
                    numberOfDays = 28;
                break;
            case "March": case "Mar.": case "Mar": case "3": 
                numberOfDays = 31;
                break;
            case "April": case "Apr.": case "Apr": case "4": 
                numberOfDays = 30;
                break;
            case "May": case "5": 
                numberOfDays = 31;
                break;
            case "June": case "Jun": case "6": 
                numberOfDays = 30;
                break;
            case "July": case "Jul": case "7": 
                numberOfDays = 31;
                break;
            case "August": case "Aug.": case "Aug": case "8": 
                numberOfDays = 31;
                break;
            case "September": case "Sep": case "Sep.": case "9": 
                numberOfDays = 30;
                break;
            case "October": case "Oct.": case "Oct": case "10": 
                numberOfDays = 31;
                break;
            case "November": case "Nov.": case "Nov": case "11": 
                numberOfDays = 30;
                break;
            case "December": case "Dec.": case "Dec": case "12": 
                numberOfDays = 31;
                break;
            default:
            	System.out.println("Invalid value!\nTry again");
        }
        System.out.println("This month has " + numberOfDays + " days");
        keyboard.close();
    }
	
	public static boolean checkLeapYear(int year) {
		boolean isLeapYear = false;
		if(year % 4 == 0 && !(year % 100 == 0 && year % 400 != 0)) 
			isLeapYear = true;
		return isLeapYear;
	}
}