import java.util.Scanner;

public class DrawingTriangle {

	public static void main(String[] args) {
		System.out.println("Enter the height of the triangle: ");
		Scanner keyboard = new Scanner(System.in);
		int n = keyboard.nextInt();
		
		System.out.println("This is your triangle:");
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=n-i+1; j++)
				System.out.print(" ");
			for (int j=1; j<=i*2-1; j++)
				System.out.print("*");
			System.out.print("\n");
		}
		keyboard.close();
	}

}
