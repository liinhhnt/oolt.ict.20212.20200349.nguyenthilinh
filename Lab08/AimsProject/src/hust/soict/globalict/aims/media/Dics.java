package hust.soict.globalict.aims.media;

public class Dics extends Media {
    private int length;
    private String director;

    public Dics() {
        super("", "", "", 0.0f);
        this.director = "";
    	this.length = 0;
    }

    public Dics(String title) {
		super("", title, "", 0.0f);
		//this.title = title;
	}

	public Dics(String title, String category) {
		super("",title, category, 0.0f);
		//this.title = title;
		//this.category = category;
	}

	public Dics(String title, String category, String director) {
		super("",title, category, 0.0f);
		//this.title = title;
		//this.category = category;
		this.director = director;
	}

	public Dics(String title, String category, String director, int length, float cost) {
        super("",title, category, cost);
		//this.title = title;
        //this.category = category;
        this.director = director;
        this.length = length;
        //this.cost = cost;
    }

    public Dics(String id, String title, String category, String director, int length, float cost) {
        super(id, title, category, cost);
		//this.title = title;
        //this.category = category;
        this.director = director;
        this.length = length;
        //this.cost = cost;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getDirector() {
        return this.director;
    }

    public void setDirector(String director) {
        this.director = director;
    }


}
