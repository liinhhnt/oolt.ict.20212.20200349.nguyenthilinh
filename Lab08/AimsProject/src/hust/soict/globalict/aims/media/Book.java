package hust.soict.globalict.aims.media;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;
import java.util.Arrays;

public class Book extends Media{
    //private String title;
    //private String category;
    //private float cost;
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    private List<String> authors = new ArrayList<String>();

    public Book(String id, String title, String category, float cost, List<String> authors) {
        super(id, title, category, cost);
        //this.title = title;
        //this.category = category;
        //this.cost = cost;
        this.authors = authors;
    }

    public Book(Book book) {
        super(book.getId(), book.getTitle(), book.getCategory(), book.getCost());
        this.authors = book.getAuthors();
    }

    public void addAuthor(String authorName) {
        for(String author: this.authors) {
            if (author.equals(authorName)) {
                System.out.println("This author has already been in the authorslist. Cannot add!");
                return ;
            } 
        } 
        this.authors.add(authorName);
        return ;       
    }

    public void removeAuthor(String authorName) {
        int flag = 0;
        for(String author: this.authors) {
            if (author.equals(authorName)) {
                this.authors.remove(authorName);
                System.out.println("This author has already been removed!");
                flag = 1;
            } 
        }
        if (flag == 0) System.out.println("Cannot find the author's name in this list!");
        return ;
    }

    public List<String> getAuthors() {
        return this.authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}

	public int getContentLength() {
		return contentTokens.size();
	}
    
	public List<String> getContentTokens() {
		return contentTokens;
	}
	
	public Map<String, Integer> getWordFrequency() {
		return wordFrequency;
	}
	
    public void processContent() {
    	contentTokens = Arrays.asList(content.split("[^a-zA-Z]+"));
    	//Collections.sort(contentTokens, String.CASE_INSENSITIVE_ORDER);
    	for (String e: contentTokens) {
    		//System.out.print(e + ' ');
    		if (wordFrequency.containsKey(e))
    			wordFrequency.replace(e, wordFrequency.get(e)+1);
    		else wordFrequency.put(e, 1);
    	}
    	//UserComparator comparator = new UserComparator(wordFrequency);
	   // Map<Integer, Integer> result = new TreeMap<Integer, Integer>(comparator);
    }

    @Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		
		result.append("Book ");
		result.append(getId());
		result.append("\n\t");
		
		result.append("Title: ");
		result.append(getTitle());
		result.append("\n\t");
		
		result.append("Category: ");
		result.append(getCategory());
		result.append("\n\t");
		
		result.append("Cost: ");
		result.append(getCost());
		result.append("\n\t");
		
		result.append("Authors: ");
		result.append(getAuthors());
		result.append("\n\t");
		
		result.append("Content length: ");
		result.append(getContentLength());
		result.append("\n\t");
		
		result.append("Token list: ");
		result.append(getWordFrequency().keySet());
		result.append("\n\t");
		
		result.append("Word frequency: ");
		result.append(getWordFrequency());
		result.append("\n\t");
		
		return result.toString();
	}
}