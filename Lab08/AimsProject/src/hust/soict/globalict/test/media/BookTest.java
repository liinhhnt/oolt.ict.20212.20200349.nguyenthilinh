package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import hust.soict.globalict.aims.media.Book;

public class BookTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Collection<Book> lstBooks = new ArrayList<Book>();
		List<String> author1 = new ArrayList<String>();
		author1.add("NTLiinhh");
		author1.add("Liinhh");
		Book book1 = new Book("1", "Harry Potter", "Scientific", 18, author1);
		lstBooks.add(book1);
		
		//test processContent method
		book1.setContent("This is the first book of the book store and also the first book of author NTLiinhh");
		//test toString() method
		System.out.print(book1.toString());
	}

}
