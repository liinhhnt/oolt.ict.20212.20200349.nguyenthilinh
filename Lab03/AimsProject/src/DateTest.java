
public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//the current date
		MyDate date1 = new MyDate();
		date1.print();
		
		//the date with 3 parameters
		MyDate date2 = new MyDate(5, 5, 2022);
		date2.print();
		
		//the date with string parameters
		MyDate date3 = new MyDate("May 05 2022");
		date3.print();
		
		//the date inputed from the keyboard
		MyDate date4 = new MyDate().accept();
		date4.print();
	}

}
