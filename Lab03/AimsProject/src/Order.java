
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	
	private DigitalVideoDisc itemOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];

	public int qtyOrdered = 0;
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if (qtyOrdered < MAX_NUMBERS_ORDERED) {
			itemOrdered[qtyOrdered] = disc;
			qtyOrdered = qtyOrdered + 1;
			System.out.print("This disc has been added!\n");
		}
		else System.out.print("The order is almost full!\n");
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		if (itemOrdered[qtyOrdered-1] == disc) {
			--qtyOrdered;
			System.out.print("This disc has been removed\n");
			return;
		}
		if (qtyOrdered > 0) {
			for (int i=0; i<qtyOrdered; i++) 
				if (itemOrdered[i] == disc) {
					itemOrdered[i] = itemOrdered[--qtyOrdered]; 
					System.out.print("This disc has been removed\n");
					return;
				}
		}
		System.out.print("This disc does not have in your order\n");
	}
	
	public float totalCost() {
		float sum = 0;
		for (int i=0; i<qtyOrdered; i++ )  {
			sum = sum + itemOrdered[i].getCost();
		}
		return sum;
	}
}
