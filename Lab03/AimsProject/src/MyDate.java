import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MyDate {
	private int day, month, year;
	
	public MyDate() {
		LocalDate currentDate = LocalDate.now();
		this.day = currentDate.getDayOfMonth();
		this.month = currentDate.getMonthValue();
		this.year = currentDate.getYear();
	}
	
	public MyDate(int day, int month, int year)
	{
		//super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate (String date) 
	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM dd yyy");
		ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, formatter);
		this.day = zonedDateTime.getDayOfMonth();
		this.month = zonedDateTime.getMonthValue();
		this.year = zonedDateTime.getYear();
	}

	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public MyDate accept()
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Enter a date (Eg: Jun 1 2022)");
		String dateString = keyboard.nextLine();
		MyDate date = new MyDate(dateString);
		return date;
	}
	
	public void print() 
	{
		System.out.println("The date today is " + day + " " + month + " " + year );
	}
	
}
